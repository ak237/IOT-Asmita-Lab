[IOT STUDENT HOME](https://gitlab.com/iot110/iot110-student/blob/master/README.md)

## Setting up Lab4

### Objectives
For this lab we will continue building on our IoT suite by adding our first
active sensor device.  We will be adding the *Bosch BMP280* Pressure/Temperature
sensor and interfacing to it using the I2C serial protocol standardized by
Philips (NXP) Semiconductor.  

The various steps of this lab are summarized as:
* [PART A](https://gitlab.com/iot110/iot110-student/blob/master/Labs/Lab4/PartA.md) Enable I2C Bus and Test Connection to BMP280
* [PART B](https://gitlab.com/iot110/iot110-student/blob/master/Labs/Lab4/PartB.md) Create BMP280 Python Driver
* [PART C](https://gitlab.com/iot110/iot110-student/blob/master/Labs/Lab4/PartC.md) Create BMP280 Python Driver Unit Test
* [PART D](https://gitlab.com/iot110/iot110-student/blob/master/Labs/Lab4/PartD.md) Test Everything

[IOT STUDENT HOME](https://gitlab.com/iot110/iot110-student/blob/master/README.md)
